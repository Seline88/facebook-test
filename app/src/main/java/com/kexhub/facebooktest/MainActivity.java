package com.kexhub.facebooktest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        final LoginButton loginButton = (LoginButton) findViewById(R.id.facebookLoginButton);
        final TextView loginResponseTextView = (TextView) findViewById(R.id.loginResponseTextView);
        Button requestButton = (Button) findViewById(R.id.requestButton);
        final TextView responseTextView = (TextView) findViewById(R.id.responseTextView);

        if (loginButton != null) {
            loginButton.setReadPermissions("read_mailbox", "user_posts");
            loginButton.registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            Log.e(LOG_TAG, "onSuccess");
                            Log.e(LOG_TAG, loginResult.toString());
                            if (loginResponseTextView != null) {
                                loginResponseTextView.setText(loginResult.toString());
                            }
                        }

                        @Override
                        public void onCancel() {
                            Log.e(LOG_TAG, "onCancel");
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            Log.e(LOG_TAG, "onError");
                            Log.e(LOG_TAG, exception.toString());
                            if (loginResponseTextView != null) {
                                loginResponseTextView.setText(exception.toString());
                            }
                        }
                    }
            );
        }
        if (requestButton != null) {
            requestButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GraphRequest graphRequest = new GraphRequest(
                            AccessToken.getCurrentAccessToken(),
                            "/me/inbox",
                            null,
                            HttpMethod.GET,
                            new GraphRequest.Callback() {
                                public void onCompleted(GraphResponse response) {
                                    Log.e(LOG_TAG, "response: " + response);
                                    if (responseTextView != null) {
                                        responseTextView.setText(response.toString());
                                    }
                                }
                            }
                    );
                    graphRequest.executeAsync();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
